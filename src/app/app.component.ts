import { Component } from '@angular/core';
import { ConfigService } from './config.service';
import {BreakpointObserver, LayoutModule} from '@angular/cdk/layout';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bulldog-player';
  menuData: [any];
  subMenu: any;
  resource: any;
  isMinimized = false;
  isImage = false;
  videoURL: string;
  showToolbarSideMenu = false;
  isSmallScreen = false;
  
  //videoURL = "https://csufresno.hosted.panopto.com/Panopto/Pages/Embed.aspx?id=e500f4b1-845b-4494-b099-acd3017c4ece&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all";

  constructor(private configService: ConfigService, breakpointObserver: BreakpointObserver){
    this.configService.getMenu().subscribe(data=>{
      this.menuData = data;
      this.subMenu = data && data.length && data[0].submenu && data[0].submenu.length ?  data[0].submenu[0]: {};
      this.resource = this.subMenu && this.subMenu.resources && this.subMenu.resources.length ? this.subMenu.resources[0]: {};
      this.videoURL = this.subMenu && this.subMenu.resources && this.subMenu.resources.length ? this.subMenu.resources[0].link: '';
      this.isSmallScreen = breakpointObserver.isMatched('(max-width: 600px)');
      if(this.isSmallScreen){
        this.isMinimized = true;
        window.screen.orientation.lock('portrait');
      }
      console.log("Is Small screen", this.isSmallScreen);
    });
  }

  // Note to all the iframe links add &showtitle=false

  onToggleMinimize(){
    this.isMinimized = !this.isMinimized;
  }

  onMenuSelect(menu, submenu, index){
    this.subMenu = submenu;
    this.resource = this.subMenu && this.subMenu.resources && this.subMenu.resources.length ? this.subMenu.resources[0]: {};
    this.videoURL = this.subMenu && this.subMenu.resources && this.subMenu.resources.length ? this.subMenu.resources[0].link: '';
    //this.toggleToolbarSideMenu();
  }

  onThumbnailSelect(isImage, link, resource){
    this.isImage = isImage;
    this.videoURL = link
    this.resource = resource;
  }

  toggleToolbarSideMenu(){
    this.showToolbarSideMenu = !this.showToolbarSideMenu; 
  }

}
