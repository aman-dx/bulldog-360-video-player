import { Component, Inject, Input, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  @Input() 
  get data(){
    return this._data;
  }
  set data(_data){
    this._data = _data;
    if(this._data && this._data.images){
      this.startindex = 0;
      this.size = this._data.images.length ? this._data.images.length: 0;
      this.batch = this.getBatch();
      this.currentIndex = this.batch.length - 1;
    }
  }

  _data:any;

  showCarousel= false;

  startindex = 0;

  currentIndex = 0;

  stepSize=3;

  size = 0;

  batch = [];

  constructor(public dialog: MatDialog) {
    
  }

  ngOnInit(): void {
    
  }

  onClick(){
    this.showCarousel = !this.showCarousel;
  }

  OnNext(){
    this.batch = this.getNextBatch();
  }

  OnPrevious(){
    this.batch = this.getPreviousBatch();
  }

  getBatch(){
    let steps = this.stepSize;
    let arr = [];
    for(let i = this.startindex; i < this.size; i++){
      if(this.data.images[i]){
        steps -= 1;
        arr.push(this.data.images[i]);
      }
      if(steps <= 0) break;
    }
    return arr;
  }

  getNextBatch(){
    let arr = [];
    arr = this.batch;
    if(this.data.images[this.currentIndex + 1]){
      this.currentIndex += 1;
      arr.push(this.data.images[this.currentIndex]);
      if(arr.length > 3){
        arr.splice(0, 1);
      }
    }
    return arr;
  }

  getPreviousBatch(){
    let arr = [];
    let steps = this.stepSize;
    if(this.currentIndex > 2){
      this.currentIndex -= 1;
    }
    for(let i = this.currentIndex; i >= 0; i -- ){
      steps -= 1;
      arr.push(this.data.images[i]);
      if(steps <= 0) break;
    }
    arr = arr.reverse();
    return arr;
  }

  openDialog(url) {
    this.dialog.open(CarouselPopupDialogue, {
      data: {
        url: url
      }
    });
  }

}


@Component({
  selector: 'carousel-popup-dialogue',
  templateUrl: 'carousel-popup-dialogue.html',
  styleUrls: ['./carousel-popup-dialogue.scss']
})
export class CarouselPopupDialogue {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<CarouselPopupDialogue>,) {}

  closeDialogue(){
    this.dialogRef.close();
  }
}