import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from './material.module';
import { DefaultPlayerComponent } from './default-player/default-player.component';
import { SafeURLPipe } from './safe-url.pipe';
import { ThumbnailComponent } from './thumbnail/thumbnail.component';
import { CarouselComponent, CarouselPopupDialogue } from './carousel/carousel.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    DefaultPlayerComponent,
    SafeURLPipe,
    ThumbnailComponent,
    CarouselComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  entryComponents:[
    CarouselPopupDialogue
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
