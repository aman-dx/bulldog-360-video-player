import { Component, Input,  Output, EventEmitter, OnInit, Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { SimpleChanges } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: ['./thumbnail.component.scss']
})
export class ThumbnailComponent implements OnInit {

  @Input() data: any;

  @Output() onThumbnailEvent = new EventEmitter<string>();

  thumbnail: string;

  constructor( @Inject(DOCUMENT) private document: Document) { 
   
  }

  ngOnInit(): void {
  }

  generatePoster(videoURL: string): Promise<string> {
    const video: HTMLVideoElement = this.document.createElement('video');
    const canvas: HTMLCanvasElement = this.document.createElement('canvas');
    const context: CanvasRenderingContext2D = canvas.getContext('2d');
    return new Promise<string>((resolve, reject) => {
      canvas.addEventListener('error',  reject);
      video.addEventListener('error',  reject);
      // video.addEventListener('canplay', event => {
      //   canvas.width = video.videoWidth;
      //   canvas.height = video.videoHeight;
      //   context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
      //   resolve(canvas.toDataURL());
      // });
      canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
        resolve(canvas.toDataURL());
      video.preload = 'auto';
      video.src = videoURL;
      video.load();
    });
  }


  onThumbnailSelect(isImage, videoURL){
    this.onThumbnailEvent.emit(videoURL);
  }

  //console.log("Error occured while genarating thumbnail video poster", e)
  ngOnChanges(changes: SimpleChanges){
    if (changes['data']) {
      try{
        this.generatePoster(this.data.link).then(posterLink=>{
          this.thumbnail = posterLink;
        });
      }catch(e){
        console.log("Error occured while genarating thumbnail video poster", e)
      };
    }
  }

}
