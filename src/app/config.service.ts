import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  configUrl = "assets/videos.config.json";

  constructor(private http: HttpClient) { }

  getConfig() {
    return this.http.get(this.configUrl);
  }

  getMenu(){
    return this.http.get(this.configUrl).pipe(map((d: any) =>{
        let menu = _.sortBy(d.menu, 'order');
        menu.map(m=>{
          if(m.submenu && m.submenu.length){
            m.submenu = _.sortBy(m.submenu, 'order');
            m.submenu.map(s => {
              if(s.resources && s.resources.length){
                s.resources = _.sortBy(s.resources, 'order');
                s.resources.map(r=>{
                  if(r.images && r.images.length){
                    r.images = _.sortBy(r.images, 'order');
                  }
                })
              }
            })
          } 

        })
        return menu;
    }));
  }
}
