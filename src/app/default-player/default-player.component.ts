import { AfterViewInit, ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-default-player',
  templateUrl: './default-player.component.html',
  styleUrls: ['./default-player.component.scss']
})
export class DefaultPlayerComponent implements OnInit, AfterViewInit  {

  @Input() link: string;
  @Input() isImage: boolean;
// Note to all the iframe links add &showtitle=false
  @ViewChild('iframe') iframe: ElementRef

  constructor(@Inject(DOCUMENT) private document: Document, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    
  }

  onLoad(){
    console.log("Iframe loaded");
    let refe = this.document.querySelector('#eventOverlayGutter');
    //let ref = this.iframe.nativeElement.contentDocument.document.getElementById('eventOverlayGutter');
    //console.log(this.iframe.nativeElement);
  }

  onError(){
    console.log("Iframe loading error");
  }


  ngAfterViewInit() {
    this.iframe.nativeElement.onload = this.onLoad();
    this.iframe.nativeElement.onerror = this.onError();
    //let ref = this.iframe.nativeElement.contentDocument.querySelector('eventOverlayGutter');
  }

  ngOnChanges(){
    if(this.link){
      //let url = this.sanitizer.bypassSecurityTrustUrl(this.link);
      this.iframe.nativeElement.src = this.link;
    }
  }

  
}
