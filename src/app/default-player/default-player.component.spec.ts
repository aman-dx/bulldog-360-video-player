import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultPlayerComponent } from './default-player.component';

describe('DefaultPlayerComponent', () => {
  let component: DefaultPlayerComponent;
  let fixture: ComponentFixture<DefaultPlayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefaultPlayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
